<?php
	require_once('includes/config.php');

	/* Check login */
	$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
	
	/* No session, so login */
	if(!isset($_SESSION["keyTicket_$myHash"]))
	{ 
		header("Location: ../login");
	}

	/* Ticket ID & Status */
	$tid = $_GET['id'];
	$status = $_GET['status'];

	$theTicket = $_GET['t'];
	$theHash = $_GET['h'];

	$stmt = $dbh->prepare("UPDATE support_tickets SET status = :status WHERE id = :tid");
	$stmt->bindParam(':status', $status);
	$stmt->bindParam(':tid', $tid);
	$stmt->execute();
	
	if ($status == 'closed')
	{
		/* Message when closing tickets */
  		$closingTicket = "The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.";
		
		/* Insert the closing message */
		$data = array( 'on_ticket' => $tid, 'from_name' => 'Admin', 'from_email' => $stemail, 'content' => $closingTicket );
		$stmt = $dbh->prepare("INSERT INTO support_messages (on_ticket, from_name, from_email, content) VALUES (:on_ticket, :from_name, :from_email, :content)");
		$stmt->execute($data);
	}

	header("Location: index.php?t=$theTicket&h=$theHash");
?>