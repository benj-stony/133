<?php
	require('includes/config.php');

	if (!empty($_GET['logout']))
	{
		$_SESSION = array();
		session_destroy();
	}

	if (!empty($_SESSION['Username']))
	{
		if ($_SESSION['Admin']==1)
		{
			header("Location: admin/index.php");
		}
		header("Location: tickets.php");
	}

	if(isset($_POST['username']) && isset($_POST['password']))
	{
		/* Escape the username, and hash the password */
		$myUser = $_POST['username'];
		$myPass = sha1($_POST['password']);

		$stmt = $dbh->prepare("SELECT username, passworddata, admin FROM support_users WHERE username = :username AND passworddata = :password");
		$stmt->bindParam(':username', $myUser);
		$stmt->bindParam(':password', $myPass);
		$stmt->execute();
		$loginDetails = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$adminUser = $loginDetails[0]['admin'];
		if (count($loginDetails) == 1 && $myUser == $loginDetails[0]['username'] && $myPass == $loginDetails[0]['passworddata'])
		{
			$_SESSION['Username'] = $myUser;
			$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
			$_SESSION["keyTicket_$myHash"] = md5(sha1($_SESSION['Username']));

			if ($adminUser == 1)
			{
				/* Can view the admin area */
				$_SESSION['Admin'] = 1;
				header("Location: admin/index.php");
			}
			else
			{
				/* Area to view the users tickets */
				header("Location: tickets.php");
			}
		}
		else
		{
			$logError = 'Username &amp; Password do not match.';
			echo $loginDetails['username'];
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Login</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
           <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">
	</head>
	<body>

		<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    <div class="item">
        <a class="ui <?php echo $themecolor; ?> button modalHandle">Sign In</a>
    </div>
        <div class="item mobile hidden">
        <a href="submit">Submit a ticket</a>
    </div>
  </div>
</div>


      <div class="ui text container centercenter ">
        	<?php
					if(isset($logError))
					{
						echo '<div class="ui red message"><strong>Error</strong> ' . $logError . '</div>';
					}
				?>
        <form method="post" action="" class="ui large form">
          <div class="field">
            <div class="ui left icon input">
              <i class="user icon"></i><input name="username" placeholder="Username..." type="text" />
            </div>
          </div>
          <div class="field">
            <div class="ui left icon input">
              <i class="lock icon"></i><input name="password" placeholder="Password..." type="password" />
            </div>
          </div>

          <button type="submit" class="ui fluid large blue submit button">
            Sign in
          </button>
        </form>
      </div>

    	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>


	    <style type="text/css">
      body {
        background-color: #eee;
      }
            .ui.footer.segment {
        width: 100%;
        margin-bottom: 0;
        margin-top:0;
        padding:20px;
        background-color: #1b1c1d;
        position: fixed;
        bottom: 0;
      }
    </style>
		   <script>
      $(document)
        .ready(function() {
          $('.ui.checkbox').checkbox();
          $('.ui.form')
            .form({
              fields: {
                email: {
                  identifier  : 'username',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your username'
                    }
                  ]
                },
                password: {
                  identifier  : 'password',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your password'
                    },
                    {
                      type   : 'length[4]',
                      prompt : 'Your password must be at least 4 characters'
                    }
                  ]
                }
              },
              inline: true,
              on: 'blur'
            })
          ;
        })
      ;
    </script>
	</body>
</html>
