<?php
	require('includes/config.php');
	include 'includes/lang/' . $myLang . '.php';

	if (empty($_POST))
	{
		header("Location: submit");
	}

	$cError = 0;
	$fError = 0;
	$lError = 0;


	/* One of the form fields is empty */
	if (empty($_POST['name']) || empty($_POST['email']) || empty($_POST['subject']) || empty($_POST['message']) || !isEmail($_POST['email']))
	{
		$fError = 1; //Field error
	}

	if (strlen($_POST['subject']) > 50 || strlen($_POST['message']) > 500)
	{
		$lError = 1; //Length error
	}

	/* If no errors, create the ticket */
	if (!$cError && !$fError && !$lError)
	{
		/* Functions can be found in config.php */
		$ticketName = hs($_POST['name']);
		$ticketEmail = hs($_POST['email']);
		$ticketSubject = hs($_POST['subject']);
		$ticketMessage = hs($_POST['message']);
		$ticketDate = date("r");
		$importancy = $_POST['importancy'];






		if (empty($_SESSION['Username']))
		{
			/* Generate username & password */
			$userName = genName();
			$passWord = genName();
			$hashPass = sha1($passWord);

			/* Create this new user and log them in */
			$data = array( 'username' => $userName, 'passworddata' => $hashPass, 'admin' => '0' );
			$stmt = $dbh->prepare("INSERT INTO support_users (username, passworddata, admin) VALUES (:username, :passworddata, :admin)");
			$stmt->execute($data);
		}
		else
		{
			/* Already logged in? Keep your username :) */
			$userName = $_SESSION['Username'];
		}

		/* New Ticket Insert */
		$data = array( 'subject' => $ticketSubject, 'date' => $ticketDate, 'status' => 'open', 'username' => $userName, 'importancy' => $importancy  );
		$stmt = $dbh->prepare("INSERT INTO support_tickets (subject, date, status, username, importancy) VALUES (:subject, :date, :status, :username, :importancy)");
		$stmt->execute($data);

		/* Ticket Info Insert */
		$data = array( 'subject' => $ticketSubject, 'date' => $ticketDate, 'status' => 'open' );
		$stmt = $dbh->prepare("SELECT * FROM support_tickets WHERE subject = :subject AND date = :date AND status = :status ORDER BY id DESC");
		$stmt->execute($data);
		$gInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$ticketID = $gInfo[0]['id'];

		/* Create the hash used to verify the ticket */
		$simpleHash = sha1($ticketID . $companyName);
		$tKey = base64_encode($ticketID);

		/* Add Message */
		$data = array( 'on_ticket' => $ticketID, 'from_name' => $ticketName, 'from_email' => $ticketEmail, 'content' => $ticketMessage );
		$stmt = $dbh->prepare("INSERT INTO support_messages (on_ticket, from_name, from_email, content) VALUES (:on_ticket, :from_name, :from_email, :content)");
		$stmt->execute($data);


		/* Send the admin an email(if enabled) and send the user an email, showing their link & user */
		if ($adminEmail)
		{
			mail($keyEmail, "Support Ticket Update, ID: " . $ticketID, "Hi,\n\nThere has been a new ticket created from your Support Desk\n\nView:\n" . $path . "admin/ticket.php?id=$ticketID", "From: " . $companyName . "<do-not-reply@donotreply.com>");
		}

		mail($ticketEmail, "Support Ticket Created ID: " . $ticketID, "Hi,\n\nThanks for sending us a ticket, we will try and get back to you as fast as possible.\n\nHere are your details for future reference:\nUser: " . $userName . "\nPass: " . $passWord . "\nTicket URL: " . $path . 'index.php?t=' . $tKey . '&h=' . $simpleHash);
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Support Center</title>
		 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
		<link type="text/css" rel="stylesheet" href="css/styles.css" />
	</head>
	<body>
	<div class="ui text container centercenter">
		<div class="ui segment blue stacked">
		<?php
			if (!$cError && !$fError && !$lError) //No errors
			{
				echo '<h3>Thanks for your request!</h3>';
				echo '<p>We have recieved your ticket, and we will reply as soon as possible!</p>';
				echo '<p>You can view the status of your ticket by clicking <a href="' . $path . 'index.php?t=' . $tKey . '&amp;h=' . $simpleHash . '">here</a></p>';
				echo '<p>To see all of your tickets click <a href="tickets">here</a>';

				if (empty($_SESSION['Username']))
				{
					echo '<p>You may also <a href="login">login</a> to keep track of all your tickets.</p>';
					echo '<p>Username: ' . $userName . '<br />';
					echo 'Password: ' . $passWord . '</p>';
				}

			}
			elseif ($fError) /* Field error */
			{
				echo '<h3>Field Error</h3>';
				echo '<p>Please make sure all fields are filled in with valid information, you can <a href="submit">go back and try again</a> if you like!</p>';
			}
			elseif ($lError) /* Length error */
			{
				echo '<h3>Too much text!</h3>';
				echo '<p>Your subject or message contains too much text! You can <a href="submit">go back and try again</a> if you like!</p>';
			}
		?>
		</div>
		</div>
	</body>
</html>
