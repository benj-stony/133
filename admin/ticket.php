<?php
	require_once('../includes/config.php');
		include '../includes/lang/' . $myLang . '.php';

	/* Check login */
	$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
	
	/* No session, so login */
	if(!isset($_SESSION["keyTicket_$myHash"]) || !isset($_SESSION['Admin']))
	{ 
		header("Location: ../login");
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Support Center</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
         <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">
    <style>

.ui.admin.message{
background-color: #e5f9e7;
    color: #1ebc30;
    box-shadow: 0 0 0 1px #1ebc30 inset, 0 0 0 0 transparent;
}
.ui.customer.message{
  
}
    </style>
	</head>
	
	<body oncontextmenu="return true">
	
	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    				<?php
					
						echo '    <div class="item">
        <a href="'.$path.'admin/index" class="ui '.$themecolor.' button">Dashboard</a>
    </div> <div class="item mobile hidden">
        <a class="modalHandle">Sign Out</a>
    </div>';
					
				?>
  </div>
</div>
	
	<div class="ui inverted vertical masthead center aligned segment <?php echo $themecolor; ?>">


    <div class="ui text container">
      <h1 class="ui inverted header">
       <?php echo $companyName; ?>
      </h1>
				<?php
					
						echo '<a href="index" class="ui white button">
<i class="icon arrow left"></i> Back to all tickets
</a>';
				
				?>
    </div>

  </div>
	

	
	
	<div class="ui vertical stripe segment">
  <div class="ui container">
  
  <div class="ui computer reversed stackable grid">
  <div class="six wide column">
  
 <!--   <div class="six wide column">
  <div class="ui segment">


  </div> 
  </div>-->
  
  <div class="ui segment piled <?php echo $themecolor; ?>">
  		<?php
			
			if (!empty($_GET['delete']))
			{
				$fileName = $_GET['delete'];
				$stmt = $dbh->prepare("DELETE FROM support_files WHERE newfile = :newfile");
				$stmt->bindParam(':newfile', $fileName);
				$stmt->execute();
				unlink('../' . $uploadPath . $fileName);
			}
		
			if ($Attachments==1)
			{
				$ticketNo = $_GET['id'];

				echo '<div id="sidebar">';
				echo '<h4><strong>File(s) Available</strong></h4>';
				echo '<div class="ui list">';
					
				$stmt = $dbh->prepare("SELECT id, oldfile, newfile, uploaderip, timestamp, ticket FROM support_files WHERE ticket = :ticket ORDER BY id DESC");
				$stmt->bindParam(':ticket', $ticketNo);
				$stmt->execute();
				$upload = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$rowNumber = $stmt->rowCount();
				if ($rowNumber==0)
				{
					echo '<p>No files have been uploaded</p>';
				}
				else
				{
					foreach ($upload as $file)
					{
						echo '<div class="fileHolder">';
						echo '  <div class="item">
    <a href="' . $path . $uploadPath . $file['newfile'] . '" class="header">' . $file['newfile'] . '</a>
    <a href="ticket.php?id=' . $ticketNo . '&delete=' . $file['newfile'] . '" class="ui icon button right floated">
  <i class="trash outline icon"></i>
</a>
    <div class="description">
      ' . $file['uploaderip'] . '
    </div>
';
						echo '</div></div>';	
					
					}
				}
				echo '</div>';
			}
		?>

  </div> 
  </div>
  </div>
 
  <div class="ten wide column">
    <div class="ui segment piled <?php echo $themecolor; ?>">
	<?php
$stmt = $dbh->prepare("SELECT id, subject, date, status, username FROM support_tickets WHERE id = :ticket");
					$stmt->bindParam(':ticket', $ticketNo);
					$stmt->execute();
					$myTicket = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
					if (count($myTicket) == 0 || !is_numeric($ticketNo))
					{
						echo '<p>Invalid ticket ID</p>';
						die;
					}
						
					$stmt = $dbh->prepare("SELECT id, on_ticket, from_name, from_email, content FROM support_messages WHERE on_ticket = :ticket ORDER BY id ASC");
					$stmt->bindParam(':ticket', $ticketNo);
					$stmt->execute();
					$message = $stmt->fetchAll(PDO::FETCH_ASSOC);
				?>

<?php
					if ($myTicket[0]['status'] == 'open')
					{
						echo '<a href="status.php?id=' . $myTicket[0]['id'] . '&status=closed&t=' . $_GET['t'] . '&h=' . $_GET['h'] . '" class="ui labeled icon button red right floated"><i class="toggle off icon"></i> Close Ticket</a>';
					}
					elseif ($myTicket[0]['status'] == 'closed')
					{
						echo '
						<a href="status.php?id=' . $myTicket[0]['id'] . '&status=open&t=' . $_GET['t'] . '&h=' . $_GET['h'] . '" class="ui labeled icon button green right floated">
  <i class="toggle on icon"></i>
  Open Ticket
</a>';
					}
				?>

				<p>
<div class="ui list">
  <span class="item"><strong><?php echo $l['Status']?>:</strong> <?php echo ucwords($myTicket[0]['status']); ?></span>
  <span class="item"><strong><?php echo $l['Subject']?>:</strong></b> <?php echo $myTicket[0]['subject']; ?></span>
  <span class="item"><strong><?php echo $l['From']?>:</strong></b> <?php echo $message[0]['from_name']; ?> (<?php echo $message[0]['from_email']; ?>)</span>
  <span class="item"><strong><?php echo $l['TicketOpened']?>:</strong></b> <?php echo $myTicket[0]['date']; ?></span>
</div>

				</p>
				<div class="ui section divider"></div>

				
				<h4><strong><?php echo $l['OriginalMessage']?>:</strong></h4>
				<div class="ui message tiny">
  <div class="header">
    Customer
  </div>
  <p><?php 
						echo nl2br($message[0]['content']);
					?></p>
</div>
				
			
					
				<?php
					$orid = $message[0]['id'];
					if(count($message) == 1)
					{
				?>
				
				<h4><strong><?php echo $l['Reply']?>:</strong></h4>
				<form method="post" action="reply.php" enctype="multipart/form-data">
				<div class="ui form">
  <div class="field">
    <label><?php echo $l['Message']?></label>
    <textarea name="reply"></textarea>
  </div>
  	<?php
						if ($Attachments==1)
						{
							echo '<p>Attachments:</p>';
							echo '<input type="file" name="file" class="fileBox" />';
						}
					?>
					<input type="hidden" value="<?php echo $myTicket[0]['id']; ?>" name="ticket_id" />
					<input type="hidden" value="<?php echo $message[0]['from_email']; ?>" name="to" />
   <button type="submit" class="ui submit button">Submit</button>
</div>
</form>
			
				
				<?php
					}
					else
					{
				?>
				
				<h4><strong>All <?php echo $l['Replies']?>:</strong></h4>
				<?php
					foreach ($message as $rep)
					{
						/* Load the style depending on who you are */
						if($rep['from_email'] == $stemail)
						{
							$ticketStyle = 'admin';
						}
						else
						{
							$ticketStyle = 'customer';
						}
							
						if($rep['id'] != $orid)
						{
							if ($BBCode)
							{
								echo '<div class="ui message tiny ' . $ticketStyle . '">
  <div class="header">
    ' . ucwords($ticketStyle) . '
  </div>
  <p>' . nl2br(addBBCode($rep['content'])) . '</p>
</div>';
							}
							else
							{
								echo '<div class="ui message tiny ' . $ticketStyle . '">
  <div class="header">
    ' . ucwords($ticketStyle) . '
  </div>
  <p>' . nl2br($rep['content']) . '</p>
</div>
';
							}
						}
					}
						
				?>
				
					
				<h4><strong>Reply:</strong></h4>
								<form method="post" action="reply.php" enctype="multipart/form-data">
				<div class="ui form">
  <div class="field">
    <label><?php echo $l['Message']?></label>
    <textarea name="reply" rows="4"></textarea>
  </div>
  	<?php
						if ($Attachments==1)
						{
						echo '<div class="field">';
							echo ' <label>Attachments</label>';
							echo '<input type="file" name="file" class="fileBox" />';
							echo '</div>';
						}
					?>
  <input type="hidden" value="<?php echo $myTicket[0]['id']; ?>" name="ticket_id" />
					<input type="hidden" value="<?php echo $message[0]['from_email']; ?>" name="from" />
					<input type="hidden" value="<?php echo $message[0]['from_name']; ?>" name="fromn" />
   <button type="submit" class="ui submit button">Submit</button>
</div>
</form>
				<?php
						}
			
				?>
  </div>
</div>
</div>
		
			</div>
		</div>
     
      
  </div>
</div>
	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> <?php echo $companyName; ?> | v<?php echo $stversion; ?></p>
      </div>
    </div>
    
        <div class="ui tiny modal custom_login_modal">
  <i class="close icon"></i>
  <div class="header ui center aligned custom_login_modal_header">
    See ya another time!
  </div>
  <div class="description">
    <br>
    <br>
    
				
      <a href="<?php echo $path; ?>main?logout=true" class="fluid ui large button <?php echo $themecolor; ?>">Sign out!</a>
      <br>

  </div>
</div>
<script>
  //modal
  $(".modalHandle").click(function() {
    $(".ui.modal")
      .modal({
        blurring: false,
        duration: 200
      })
      .modal("show");
  });
</script> 
	</body>
</html>