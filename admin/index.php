<?php
	require_once('../includes/config.php');

	/* Check login */
	$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));

	/* No session, so login */
	if(!isset($_SESSION["keyTicket_$myHash"]) || !isset($_SESSION['Admin']))
	{
		header("Location: ../login");
	}

	if (!empty($_GET['delete']))
	{
		$tID = $_GET['delete'];
		$stmt = $dbh->prepare("DELETE FROM support_tickets WHERE id = :id");
		$stmt->bindParam(':id', $tID);
		$stmt->execute();
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Admin</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">

	</head>

	<body oncontextmenu="return false">

	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    <div class="item">
        <a href="<?php echo $path; ?>admin/index" class="ui <?php echo $themecolor; ?> button">Dashboard</a>
    </div>
        <div class="item mobile hidden">
        <a class="modalHandle">Sign Out</a>
    </div>
  </div>
</div>

	<div class="ui inverted vertical masthead center aligned segment <?php echo $themecolor; ?>">


    <div class="ui text container">
      <h1 class="ui inverted header">
        Admin Panel
      </h1>
      <a href="#" class="ui white button">
Edit Site Settings (next update)
</a>
      <a href="#" class="ui white button">
Edit Content Blocks (next update)
</a>
     <!-- <a href="users" class="ui white button">
User Management
</a>-->
    </div>

  </div>




	<div class="ui vertical stripe segment">
  <div class="ui container">
      <h4 class="ui header blue">
  <i class="toggle on icon"></i>
  <div class="content">
    Open Tickets
  </div>
</h4>
      <table class="ui celled table">
  <thead>
    <tr>
      <th>Subject</th>
      <th>Replies</th>
			<th>Importancy</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

  				<?php
					//$tickets = mysql_query("SELECT * FROM tickets WHERE status = 'open' ORDER BY id DESC");

					$stmt = $dbh->prepare("SELECT * FROM support_tickets WHERE status = 'open' ORDER BY id DESC");
					$stmt->execute();
					$tickets = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$countOpen = count($tickets);

					if ($countOpen == 0)
					{
						echo '<div class="ticket"><b>Notice:</b> There are currently no open tickets.</div>';
					}
					else
					{
						/* Get the open tickets */
						foreach ($tickets as $ticket)
						{
							$ticket_id = $ticket['id'];

							$stmt = $dbh->prepare("SELECT * FROM support_messages WHERE on_ticket = :ticketID");
							$stmt->bindParam(':ticketID', $ticket_id);
							$stmt->execute();
							$getReplies = $stmt->fetchAll(PDO::FETCH_ASSOC);





							/* Count the rows */
							$replyNo = count($getReplies);

							/* Proper grammar is helpful! */
							if ($replyNo == 1)
							{
								$wordUsed = 'Message';
							}
							else
							{
								$wordUsed = 'Messages';
							}



							/*echo '<div class="ticket">';
							echo '<a href="ticket.php?id=' . $ticket['id'] . '">' . $ticket['subject'] . '</a>&nbsp;&nbsp; ~ &nbsp;&nbsp;' . $replyNo . ' ' . $wordUsed;
							echo '<a href="?delete=' . $ticket['id'] . '"><img style="float: right" alt="Delete Ticket" class="deleteImage" src="../images/cross-small.png"/></a></div>';*/

							         echo '<tr>';
                        echo '<td><a href="ticket.php?id=' . $ticket['id'] . '">' . $ticket['subject'] . '</a></td> <td>' . $replyNo . ' ' . $wordUsed . '</td> <td>' . $ticket['importancy']. '</td> <td><a href="?delete=' . $ticket['id'] . '">Delete</a></td>';
                        echo '</tr>';
						}
					}

				?>


  </tbody>
</table>

      	      <h4 class="ui header blue">
  <i class="toggle off icon"></i>
  <div class="content">
    Closed Tickets
  </div>
</h4>
      <table class="ui celled table">
  <thead>
    <tr>
      <th>Subject</th>
      <th>Replies</th>
			<th>Importancy</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
      					<?php
					$stmt = $dbh->prepare("SELECT * FROM support_tickets WHERE status = 'closed' ORDER BY id DESC");
					$stmt->execute();
					$tickets = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$countClosed = count($tickets);

					if ($countClosed == 0)
					{
						echo '<div class="ticket"><b>Notice:</b> There are currently no closed tickets.</div>';
					}
					else
					{
						/* Get the closed tickets */
						foreach ($tickets as $ticket)
						{
							$ticket_id = $ticket['id'];

							$stmt = $dbh->prepare("SELECT * FROM support_messages WHERE on_ticket = :ticketID");
							$stmt->bindParam(':ticketID', $ticket_id);
							$stmt->execute();
							$getReplies = $stmt->fetchAll(PDO::FETCH_ASSOC);

							/* Count the rows */
							$replyNo = count($getReplies);

							/* Proper grammar is helpful! */
							if ($replyNo == 1)
							{
								$wordUsed = 'Message';
							}
							else
							{
								$wordUsed = 'Messages';
							}

							   echo '<tr>';
                        echo '<td><a href="ticket.php?id=' . $ticket['id'] . '">' . $ticket['subject'] . '</a></td> <td>' . $replyNo . ' ' . $wordUsed . '</td> <td>' . $ticket['importancy']. '</td> <td><a href="?delete=' . $ticket['id'] . '">Delete</a></td>';
                        echo '</tr>';
						}
					}
				?>
 </tbody>
</table>




  </div>
</div>
	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> <?php echo $companyName; ?> | v<?php echo $stversion; ?></p>
      </div>
    </div>

        <div class="ui tiny modal custom_login_modal">
  <i class="close icon"></i>
  <div class="header ui center aligned custom_login_modal_header">
    See ya another time!
  </div>
  <div class="description">
    <br>
    <br>


      <a href="<?php echo $path; ?>main.php?logout=true" class="fluid ui large button <?php echo $themecolor; ?>">Sign out!</a>
      <br>

  </div>
</div>
<script>
  //modal
  $(".modalHandle").click(function() {
    $(".ui.modal")
      .modal({
        blurring: false,
        duration: 200
      })
      .modal("show");
  });
</script>
	</body>
</html>
