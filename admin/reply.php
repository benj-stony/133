<?php
	require_once('../includes/config.php');

	/* Check login */
	$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
	
	/* No session, so login */
	if(!isset($_SESSION["keyTicket_$myHash"]) || !isset($_SESSION['Admin']))
	{
		header("Location: ../login");
	}

	$tid = $_POST['ticket_id'];
	$from_name = $companyName;
	$from_email = hs($stemail);
	$message = hs($_POST['reply']);
	$to = hs($_POST['to']);
	
	/* Upload attachment if it's enabled and there is a file */
	if (!empty($_FILES) && $Attachments && !empty($_FILES['file']['tmp_name']))
	{
		$oldFile = basename($_FILES['file']['name']);
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		
		//Get the files extension
		$getExt = pathinfo($oldFile);
		$checkExt = strtolower($getExt['extension']);
		
		//Check if the files extension is okay
		if (!in_array($checkExt, $adminUploads))
		{   
			//Check for images
			echo "That file-type is not allowed.";
			die;
		}
		
		//Create the new file name
		$newFile = fileName() . "." . $checkExt;
		
		$uploadedPath = '../' . $uploadPath . $newFile;
		
		//The time
		$theTime = time();
		
		if (is_uploaded_file($_FILES['file']['tmp_name']))
		{
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadedPath))
			{
				/* Insert file info into database */
				$data = array( 'oldfile' => $oldFile, 'newfile' => $newFile, 'uploaderip' => $ipAddress, 'timestamp' => $theTime, 'ticket' => $tid );
				$stmt = $dbh->prepare("INSERT INTO support_files (oldfile, newfile, uploaderip, timestamp, ticket) VALUES (:oldfile, :newfile, :uploaderip, :timestamp, :ticket)");
				$stmt->execute($data);
			}
		}
	}
	
	if (empty($message))
	{
		$fError = 1;
	}

	$simpleHash = sha1($tid . $companyName);
	$key = base64_encode($tid);

	if (!$fError)
	{
		$umess = "Hi,\n\nYour ticket on the " . $companyName . " Support Ticket has been updated, the reply was:\n----------------------------------\n\n$message\n\n----------------------------------\n\nYou can view, and reply to this ticket at the following URL:\n$keyPath?t=$key&h=$simpleHash\n\nThanks,\n$from_name";
		
		/* Insert the reply into the database */
		$data = array( 'on_ticket' => $tid, 'from_name' => $from_name, 'from_email' => $from_email, 'content' => $message );
		$stmt = $dbh->prepare("INSERT INTO support_messages (on_ticket, from_name, from_email, content) VALUES (:on_ticket, :from_name, :from_email, :content)");
		$stmt->execute($data);

		mail($to, "$from_name Ticket Updated", $umess, "From: $from_name <no-reply@domain.com>");
		header("Location: ticket.php?id=$tid");
	}
	else
	{
		header("Location: ticket.php?id=$tid");
	}

?>