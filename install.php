<?php
	include 'includes/config.php';
	$phpVersion = phpversion();
	$phpCheck = seValid($phpVersion);
	$folderValue = substr(sprintf('%o', fileperms($uploadPath)), -4);

	$adminregister = 'on';

if ($adminregister == 'on'){
#echo 'Admin Registration is on, dont forget to disable it after you have created an account!';
}
else{
exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Install</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>

    <style>
    /* Mobile */

@media only screen and (max-width: 767px) {
  [class*="mobile hidden"],
  [class*="tablet only"]:not(.mobile),
  [class*="computer only"]:not(.mobile),
  [class*="large monitor only"]:not(.mobile),
  [class*="widescreen monitor only"]:not(.mobile),
  [class*="or lower hidden"] {
    display: none !important;
  }
}


/* Tablet / iPad Portrait */

@media only screen and (min-width: 768px) and (max-width: 991px) {
  [class*="mobile only"]:not(.tablet),
  [class*="tablet hidden"],
  [class*="computer only"]:not(.tablet),
  [class*="large monitor only"]:not(.tablet),
  [class*="widescreen monitor only"]:not(.tablet),
  [class*="or lower hidden"]:not(.mobile) {
    display: none !important;
  }
}


/* Computer / Desktop / iPad Landscape */

@media only screen and (min-width: 992px) and (max-width: 1199px) {
  [class*="mobile only"]:not(.computer),
  [class*="tablet only"]:not(.computer),
  [class*="computer hidden"],
  [class*="large monitor only"]:not(.computer),
  [class*="widescreen monitor only"]:not(.computer),
  [class*="or lower hidden"]:not(.tablet):not(.mobile) {
    display: none !important;
  }
}


/* Large Monitor */

@media only screen and (min-width: 1200px) and (max-width: 1919px) {
  [class*="mobile only"]:not([class*="large monitor"]),
  [class*="tablet only"]:not([class*="large monitor"]),
  [class*="computer only"]:not([class*="large monitor"]),
  [class*="large monitor hidden"],
  [class*="widescreen monitor only"]:not([class*="large monitor"]),
  [class*="or lower hidden"]:not(.computer):not(.tablet):not(.mobile) {
    display: none !important;
  }
}


/* Widescreen Monitor */

@media only screen and (min-width: 1920px) {
  [class*="mobile only"]:not([class*="widescreen monitor"]),
  [class*="tablet only"]:not([class*="widescreen monitor"]),
  [class*="computer only"]:not([class*="widescreen monitor"]),
  [class*="large monitor only"]:not([class*="widescreen monitor"]),
  [class*="widescreen monitor hidden"],
  [class*="widescreen monitor or lower hidden"] {
    display: none !important;
  }
}


      .hidden.menu {
      display: none;
    }
    .masthead.segment {
      min-height: 300px;
      padding: 1em 0em;
    }
    .masthead .logo.item img {
      margin-right: 1em;
    }
    .masthead .ui.menu .ui.button {
      margin-left: 0.5em;
    }
    .masthead h1.ui.header {
     margin-top: 1.6em;
      font-size: 3em;
      font-weight: normal;
    }
    .masthead h2 {
      font-size: 1.7em;
      font-weight: normal;
    }

    .ui.vertical.stripe {
      padding: 8em 0em;
    }
    .ui.vertical.stripe h3 {
      font-size: 2em;
    }
    .ui.vertical.stripe .button + h3,
    .ui.vertical.stripe p + h3 {
      margin-top: 3em;
    }
    .ui.vertical.stripe .floated.image {
      clear: both;
    }
    .ui.vertical.stripe p {
      font-size: 1.33em;
    }
    .ui.vertical.stripe .horizontal.divider {
      margin: 3em 0em;
    }

    .quote.stripe.segment {
      padding: 0em;
    }
    .quote.stripe.segment .grid .column {
      padding-top: 5em;
      padding-bottom: 5em;
    }

    .footer.segment {
      padding: 5em 0em;
    }

    .secondary.pointing.menu .toc.item {
      display: none;
    }

    @media only screen and (max-width: 700px) {
      .ui.fixed.menu {
        display: none !important;
      }
      .secondary.pointing.menu .item,
      .secondary.pointing.menu .menu {
        display: none;
      }
      .secondary.pointing.menu .toc.item {
        display: block;
      }
      .masthead.segment {
        min-height: 350px;
      }
      .masthead h1.ui.header {
        font-size: 2em;
        margin-top: 1.5em;
      }
      .masthead h2 {
        margin-top: 0.5em;
        font-size: 1.5em;
      }
    }
        .ui.footer.segment {
        width: 100%;
        margin-bottom: 0;
        margin-top:0;
        padding:20px;
        background-color: #1b1c1d;
      }
.custom_nomargin{
    margin: 0 !important;
}
.custom_blockpadding{
padding: 20px 30px !important;
}
.custom_login_modal{
    text-align: center;
    padding:30px;
}
.grad {
    background: red; /* For browsers that do not support gradients */
background: -webkit-linear-gradient(#2185d0, rgba(33, 133, 208, 0.53)) !important;
    background: -o-linear-gradient(red, yellow); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(red, yellow); /* For Firefox 3.6 to 15 */
    background: linear-gradient(red, yellow); /* Standard syntax */
}
    </style>
	</head>

	<body oncontextmenu="return false">

	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    <div class="item">
        <a class="ui <?php echo $themecolor; ?> button">Sign In</a>
    </div>
        <div class="item mobile hidden">
        <a href="submit">Submit a ticket</a>
    </div>
  </div>
</div>

	<div class="ui inverted vertical masthead center aligned segment <?php echo $themecolor; ?>">


    <div class="ui text container">
      <h1 class="ui inverted header">
        <?php echo $companyName; ?>
      </h1>
      <a href="submit" class="ui white button">
  Submit a ticket!
</a>
    </div>

  </div>

	<div class="ui vertical stripe segment">
  <div class="ui container">

 <div class="ui message tiny">
  <div class="header">
    This is the admin account setup
  </div>
  <p>We reccomend going to this file and changing the 'adminregister' status from on to off to prevent unwanted admin accounts!</p>
</div>

  <form action="" method="post" class="ui form">
  <div class="field">
    <label>Username</label>
<input type="text" name="username" id="name" required="required" placeholder="Username..."/>
  </div>
  <div class="field">
    <label>Password</label>
<input type="password" name="password" id="password" required="required" placeholder="Password..."/>
  </div>

  <button class="ui button" type="submit" name="submit">Submit</button>
</form>




	<?php
if(isset($_POST["submit"])){

try {
    $dbh= new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // <== add this line
$sql = "INSERT INTO support_users (username, password, admin)
VALUES ('".$_POST["username"]."','".sha1($_POST["password"])."','1')";
if ($dbh->query($sql)) {
echo "<script type= 'text/javascript'>alert('New Record Inserted Successfully');</script>";
}
else{
echo "<script type= 'text/javascript'>alert('Data not successfully Inserted.');</script>";
}

$dbh = null;
}
catch(PDOException $e)
{
echo $e->getMessage();
}

}
?>


      	<?php

				if ($phpCheck)
				{
					$phpVersion = '<span style="color:green;">' . $phpVersion . '</span>';
				}
				else
				{
					$phpVersion = '<span style="color:orange;">' . $phpVersion . '</span>';
				}


				if (gotPDO())
				{
					$pdoResult = '<span style="color:green;">enabled</span>';
				}
				else
				{
					$pdoResult = '<span style="color:orange;">disabled</span>';
				}

			?>
			<p>PHP Version is: <?php echo $phpVersion; ?></p>
			<p>PDO Extension is: <?php echo $pdoResult; ?></p>


  </div>
</div>
	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>


	</body>
</html>

<?php

	function seValid($vers)
	{
		if ($vers > 5.0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function gotPDO()
	{
		if (defined('PDO::ATTR_DRIVER_NAME'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
