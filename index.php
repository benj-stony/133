<?php
	require('includes/config.php');
	include 'includes/lang/' . $myLang . '.php';

	if (empty($_GET))
	{
		header("Location: main");
	}

	if (!empty($_SESSION['Username']))
	{
		$userName = $_SESSION['Username'];
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Support Center</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>

    <style>
    /* Mobile */

@media only screen and (max-width: 767px) {
  [class*="mobile hidden"],
  [class*="tablet only"]:not(.mobile),
  [class*="computer only"]:not(.mobile),
  [class*="large monitor only"]:not(.mobile),
  [class*="widescreen monitor only"]:not(.mobile),
  [class*="or lower hidden"] {
    display: none !important;
  }
}


/* Tablet / iPad Portrait */

@media only screen and (min-width: 768px) and (max-width: 991px) {
  [class*="mobile only"]:not(.tablet),
  [class*="tablet hidden"],
  [class*="computer only"]:not(.tablet),
  [class*="large monitor only"]:not(.tablet),
  [class*="widescreen monitor only"]:not(.tablet),
  [class*="or lower hidden"]:not(.mobile) {
    display: none !important;
  }
}


/* Computer / Desktop / iPad Landscape */

@media only screen and (min-width: 992px) and (max-width: 1199px) {
  [class*="mobile only"]:not(.computer),
  [class*="tablet only"]:not(.computer),
  [class*="computer hidden"],
  [class*="large monitor only"]:not(.computer),
  [class*="widescreen monitor only"]:not(.computer),
  [class*="or lower hidden"]:not(.tablet):not(.mobile) {
    display: none !important;
  }
}


/* Large Monitor */

@media only screen and (min-width: 1200px) and (max-width: 1919px) {
  [class*="mobile only"]:not([class*="large monitor"]),
  [class*="tablet only"]:not([class*="large monitor"]),
  [class*="computer only"]:not([class*="large monitor"]),
  [class*="large monitor hidden"],
  [class*="widescreen monitor only"]:not([class*="large monitor"]),
  [class*="or lower hidden"]:not(.computer):not(.tablet):not(.mobile) {
    display: none !important;
  }
}


/* Widescreen Monitor */

@media only screen and (min-width: 1920px) {
  [class*="mobile only"]:not([class*="widescreen monitor"]),
  [class*="tablet only"]:not([class*="widescreen monitor"]),
  [class*="computer only"]:not([class*="widescreen monitor"]),
  [class*="large monitor only"]:not([class*="widescreen monitor"]),
  [class*="widescreen monitor hidden"],
  [class*="widescreen monitor or lower hidden"] {
    display: none !important;
  }
}


      .hidden.menu {
      display: none;
    }
    .masthead.segment {
      min-height: 300px;
      padding: 1em 0em;
    }
    .masthead .logo.item img {
      margin-right: 1em;
    }
    .masthead .ui.menu .ui.button {
      margin-left: 0.5em;
    }
    .masthead h1.ui.header {
     margin-top: 1.6em;
      font-size: 3em;
      font-weight: normal;
    }
    .masthead h2 {
      font-size: 1.7em;
      font-weight: normal;
    }

    .ui.vertical.stripe {
      padding: 8em 0em;
    }
    .ui.vertical.stripe h3 {
      font-size: 2em;
    }
    .ui.vertical.stripe .button + h3,
    .ui.vertical.stripe p + h3 {
      margin-top: 3em;
    }
    .ui.vertical.stripe .floated.image {
      clear: both;
    }
    .ui.vertical.stripe p {
      font-size: 1.33em;
    }
    .ui.vertical.stripe .horizontal.divider {
      margin: 3em 0em;
    }

    .quote.stripe.segment {
      padding: 0em;
    }
    .quote.stripe.segment .grid .column {
      padding-top: 5em;
      padding-bottom: 5em;
    }

    .footer.segment {
      padding: 5em 0em;
    }

    .secondary.pointing.menu .toc.item {
      display: none;
    }

    @media only screen and (max-width: 700px) {
      .ui.fixed.menu {
        display: none !important;
      }
      .secondary.pointing.menu .item,
      .secondary.pointing.menu .menu {
        display: none;
      }
      .secondary.pointing.menu .toc.item {
        display: block;
      }
      .masthead.segment {
        min-height: 350px;
      }
      .masthead h1.ui.header {
        font-size: 2em;
        margin-top: 1.5em;
      }
      .masthead h2 {
        margin-top: 0.5em;
        font-size: 1.5em;
      }
    }
        .ui.footer.segment {
        width: 100%;
        margin-bottom: 0;
        margin-top:0;
        padding:20px;
        background-color: #1b1c1d;
      }
.custom_nomargin{
    margin: 0 !important;
}
.custom_blockpadding{
padding: 20px 30px !important;
}
.custom_login_modal{
    text-align: center;
    padding:30px;
}
.grad {
    background: red; /* For browsers that do not support gradients */
background: -webkit-linear-gradient(#2185d0, rgba(33, 133, 208, 0.53)) !important;
    background: -o-linear-gradient(red, yellow); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(red, yellow); /* For Firefox 3.6 to 15 */
    background: linear-gradient(red, yellow); /* Standard syntax */
}
.ui.admin.message{
background-color: #e5f9e7;
    color: #1ebc30;
    box-shadow: 0 0 0 1px #1ebc30 inset, 0 0 0 0 transparent;
}
.ui.customer.message{

}
    </style>
	</head>

	<body oncontextmenu="return true">

	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    				<?php
					if (!empty($userName))
					{
						echo '    <div class="item">
        <a href="'.$path.'admin/index" class="ui '.$themecolor.' button">Dashboard</a>
    </div> <div class="item mobile hidden">
        <a class="modalHandle">Sign Out</a>
    </div>';
					}
					else{
					echo ' <div class="item">
        <a href="'.$path.'login" class="ui '.$themecolor.' button">Sign In</a>
    </div><div class="item mobile hidden">
        <a href="'.$path.'submit" class="">Submit a ticket</a>
    </div>';
					}
				?>
  </div>
</div>

	<div class="ui inverted vertical masthead center aligned segment <?php echo $themecolor; ?>">


    <div class="ui text container">
      <h1 class="ui inverted header">
       <?php echo $companyName; ?>
      </h1>
				<?php
					if (!empty($userName))
					{
						echo '<a href="tickets.php" class="ui white button">
<i class="icon arrow left"></i> Back to tickets
</a>';
					}
					else{
					echo '<a href="submit" class="ui white button">
Create a ticket
</a>';
					}
				?>
    </div>

  </div>




	<div class="ui vertical stripe segment">
  <div class="ui container">

  <div class="ui computer reversed stackable grid">
  <div class="six wide column">

 <!--   <div class="six wide column">
  <div class="ui segment">


  </div>
  </div>-->

  <div class="ui segment piled <?php echo $themecolor; ?>">
  <?php

			$ticketNo = base64_decode($_GET['t']);
			if ($Attachments==1)
			{
				echo '<div id="sidebar">';
				echo '<h4><strong>File(s) Available</strong></h4>';
				echo '<div class="ui list">';

				$stmt = $dbh->prepare("SELECT id, oldfile, newfile, uploaderip, timestamp, ticket FROM support_files WHERE ticket = :ticket ORDER BY id DESC");
				$stmt->bindParam(':ticket', $ticketNo);
				$stmt->execute();
				$upload = $stmt->fetchAll(PDO::FETCH_ASSOC);

				$rowNumber = $stmt->rowCount();
				if ($rowNumber==0)
				{
					echo '<p>No files have been uploaded</p>';
				}
				else
				{
					foreach ($upload as $file)
					{
						echo '<div class="fileHolder">';
						echo '  <div class="item">
    <a href="' . $keyPath . $uploadPath . $file['newfile'] . '" class="header">' . $file['newfile'] . '</a>
    <div class="description">
      ' . $file['uploaderip'] . '</a>.
    </div>
';
						echo '</div></div>';

					}
				}
				echo '</div>';
			}
		?>
  </div>
  </div>
  </div>

  <div class="ten wide column">
    <div class="ui segment piled <?php echo $themecolor; ?>">
	<?php
					$stmt = $dbh->prepare("SELECT id, subject, date, status, username FROM support_tickets WHERE id = :ticket");
					$stmt->bindParam(':ticket', $ticketNo);
					$stmt->execute();
					$myTicket = $stmt->fetchAll(PDO::FETCH_ASSOC);

					if (count($myTicket) == 0 || !is_numeric($ticketNo))
					{
						echo '<p>Invalid ticket ID</p>';
						die;
					}

					$stmt = $dbh->prepare("SELECT id, on_ticket, from_name, from_email, content FROM support_messages WHERE on_ticket = :ticket ORDER BY id ASC");
					$stmt->bindParam(':ticket', $ticketNo);
					$stmt->execute();
					$message = $stmt->fetchAll(PDO::FETCH_ASSOC);

					$simpleHash = sha1($ticketNo . $companyName);

					/* The hash is incorrect */
					if ($_GET['h'] != $simpleHash)
					{
						header("Location: submit");
					}
				?>

<?php
					if ($myTicket[0]['status'] == 'open')
					{
						echo '<a href="status.php?id=' . $myTicket[0]['id'] . '&status=closed&t=' . $_GET['t'] . '&h=' . $_GET['h'] . '" class="ui labeled icon button red right floated"><i class="toggle off icon"></i> Close Ticket</a>';
					}
					elseif ($myTicket[0]['status'] == 'closed')
					{
						echo '
						<a href="status.php?id=' . $myTicket[0]['id'] . '&status=open&t=' . $_GET['t'] . '&h=' . $_GET['h'] . '" class="ui labeled icon button green right floated">
  <i class="toggle on icon"></i>
  Open Ticket
</a>';
					}
				?>

				<p>
<div class="ui list">
  <span class="item"><strong><?php echo $l['Status']?>:</strong> <?php echo ucwords($myTicket[0]['status']); ?></span>
  <span class="item"><strong><?php echo $l['Subject']?>:</strong></b> <?php echo $myTicket[0]['subject']; ?></span>
  <span class="item"><strong><?php echo $l['From']?>:</strong></b> <?php echo $message[0]['from_name']; ?> (<?php echo $message[0]['from_email']; ?>)</span>
  <span class="item"><strong><?php echo $l['TicketOpened']?>:</strong></b> <?php echo $myTicket[0]['date']; ?></span>
</div>

				</p>
				<div class="ui section divider"></div>
				<h4><strong><?php echo $l['OriginalMessage']?>:</strong></h4>
				<div class="ui message tiny">
  <div class="header">
    Customer
  </div>
  <p><?php
						echo nl2br($message[0]['content']);
					?></p>
</div>



				<?php
					$messageID = $message[0]['id'];
					if(count($message) == 1 && $myTicket[0]['status'] == 'open')
					{
				?>

				<h4><strong><?php echo $l['Reply']?>:</strong></h4>
				<form method="post" action="reply.php" enctype="multipart/form-data">
				<div class="ui form">
  <div class="field">
    <label><?php echo $l['Message']?></label>
    <textarea name="reply"></textarea>
  </div>
  	<?php
						if ($Attachments==1)
						{
							echo '<p>Attachments:</p>';
							echo '<input type="file" name="file" class="fileBox" />';
						}
					?>
  <input type="hidden" value="<?php echo $myTicket[0]['id']; ?>" name="ticket_id" />
					<input type="hidden" value="<?php echo $message[0]['from_email']; ?>" name="from" />
					<input type="hidden" value="<?php echo $message[0]['from_name']; ?>" name="fromn" />
   <button type="submit" class="ui submit button">Submit</button>
</div>
</form>


				<?php
					}
					else
					{
				?>

				<h4><strong>All <?php echo $l['Replies']?>:</strong></h4>
				<?php
					foreach ($message as $rep)
					{
						/* Load the style depending on who you are */
						if($rep['from_email'] == $stemail)
						{
							$ticketStyle = 'admin';
						}
						else
						{
							$ticketStyle = 'customer';
						}

						if($rep['id'] != $messageID)
						{
							if ($BBCode)
							{
								echo '<div class="ui message tiny ' . $ticketStyle . '">
  <div class="header">
    ' . ucwords($ticketStyle) . '
  </div>
  <p>' . nl2br(addBBCode($rep['content'])) . '</p>
</div>';
							}
							else
							{
								echo '<div class="ui message tiny ' . $ticketStyle . '">
  <div class="header">
    ' . ucwords($ticketStyle) . '
  </div>
  <p>' . nl2br($rep['content']) . '</p>
</div>
';
							}
						}
					}

					if ($myTicket[0]['status'] == 'open')
					{
				?>

				<h4><strong>Reply:</strong></h4>
								<form method="post" action="reply.php" enctype="multipart/form-data">
				<div class="ui form">
  <div class="field">
    <label><?php echo $l['Message']?></label>
    <textarea name="reply" rows="4"></textarea>
  </div>
  	<?php
						if ($Attachments==1)
						{
						echo '<div class="field">';
							echo ' <label>Attachments</label>';
							echo '<input type="file" name="file" class="fileBox" />';
							echo '</div>';
						}
					?>
  <input type="hidden" value="<?php echo $myTicket[0]['id']; ?>" name="ticket_id" />
					<input type="hidden" value="<?php echo $message[0]['from_email']; ?>" name="from" />
					<input type="hidden" value="<?php echo $message[0]['from_name']; ?>" name="fromn" />
   <button type="submit" class="ui submit button">Submit</button>
</div>
</form>
				<?php
						}
					}
				?>
  </div>
</div>
</div>

			</div>
		</div>


  </div>
</div>
	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>

        <div class="ui tiny modal custom_login_modal">
  <i class="close icon"></i>
  <div class="header ui center aligned custom_login_modal_header">
    See ya another time!
  </div>
  <div class="description">
    <br>
    <br>


      <a href="<?php echo $path; ?>login.php?logout=true" class="fluid ui large button <?php echo $themecolor; ?>">Sign out!</a>
      <br>

  </div>
</div>
<script>
  //modal
  $(".modalHandle").click(function() {
    $(".ui.modal")
      .modal({
        blurring: false,
        duration: 200
      })
      .modal("show");
  });
</script>
	</body>
</html>
