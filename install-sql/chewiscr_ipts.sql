-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 03, 2017 at 06:14 PM
-- Server version: 10.0.30-MariaDB-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chewiscr_ipts`
--

-- --------------------------------------------------------

--
-- Table structure for table `support_files`
--

CREATE TABLE IF NOT EXISTS `support_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldfile` varchar(56) NOT NULL,
  `newfile` varchar(11) NOT NULL,
  `uploaderip` varchar(32) NOT NULL,
  `timestamp` int(15) NOT NULL,
  `ticket` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `support_files`
--

INSERT INTO `support_files` (`id`, `oldfile`, `newfile`, `uploaderip`, `timestamp`, `ticket`) VALUES
(1, 'version.txt', 'dM6S4.txt', '#', 1504309732, 2);

-- --------------------------------------------------------

--
-- Table structure for table `support_info`
--

CREATE TABLE IF NOT EXISTS `support_info` (
  `id` int(11) NOT NULL,
  `box1` text NOT NULL,
  `box2` text NOT NULL,
  `box3` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `support_messages`
--

CREATE TABLE IF NOT EXISTS `support_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `on_ticket` varchar(255) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `support_messages`
--

INSERT INTO `support_messages` (`id`, `on_ticket`, `from_name`, `from_email`, `content`) VALUES
(1, '1', 'feg', 'edgr@wdt.com', 'sdfgerg'),
(2, '1', 'Support Ticket PHP Script', 'hello@domain.com', 'no worrys'),
(3, '1', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(4, '1', 'Support Ticket PHP Script', 'hello@domain.com', 'hey'),
(5, '2', 'bob', 'djk@DWfweq.com', 'xdnflednfkf3r'),
(6, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'nwkjfnfekjfksjd emfe rk wjert3jg jdw jernfrejkjwd ferf'),
(7, '2', 'bob', 'djk@DWfweq.com', 'thankd every so mucb'),
(8, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(9, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(10, '3', 'Marcus', 'steve@domain.com', 't3te22rf3'),
(11, '3', 'Support Ticket PHP Script', 'hello@domain.com', 'hi there\r\n'),
(12, '3', 'Marcus', 'steve@domain.com', 'mina myers'),
(13, '3', 'Marcus', 'steve@domain.com', 'ok thanks\r\n'),
(14, '4', 'Fish face', 'my@gw.com', 'help me'),
(15, '4', 'Support Ticket PHP Script', 'hello@domain.com', 'hi there how can i help you'),
(16, '4', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(17, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(18, '2', 'bob', 'djk@DWfweq.com', 'we have reoopened the ticket'),
(19, '2', 'bob', 'djk@DWfweq.com', 'hi'),
(20, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(21, '2', 'bob', 'djk@DWfweq.com', '&lt;p&gt;dfds&lt;/p&gt;'),
(22, '2', 'bob', 'djk@DWfweq.com', 'test'),
(23, '3', 'Support Ticket PHP Script', 'hello@domain.com', 'hiyas\r\n'),
(24, '5', 'test', 't32@Sd.com', 'wr3r4'),
(25, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'sorry we are closed'),
(26, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(27, '6', 'test', 'test@domain.com', 'test'),
(28, '7', 'test', 'test@domain.com', 'scdfffgwrgefbre'),
(29, '8', 'ghfj', 'nbvdjfd@gtck.com', 'qdvg'),
(30, '8', 'ghfj', 'nbvdjfd@gtck.com', 'efedfge'),
(31, '8', 'ghfj', 'nbvdjfd@gtck.com', 'efer'),
(32, '8', 'ghfj', 'nbvdjfd@gtck.com', 'efer'),
(33, '8', 'ghfj', 'nbvdjfd@gtck.com', 'fwefr'),
(34, '8', 'ghfj', 'nbvdjfd@gtck.com', 'dfqgfrf'),
(35, '8', 'ghfj', 'nbvdjfd@gtck.com', 'eferrewr'),
(36, '8', 'ghfj', 'nbvdjfd@gtck.com', 'erwr'),
(37, '8', 'ghfj', 'nbvdjfd@gtck.com', 'dewrtqwf'),
(38, '9', 'test', 'et3e@FEwt.com', 'efwerwrr'),
(39, '10', 'qwd', 'wqd@eqwr.com', 'e2@asr.com'),
(40, '11', 'wef', 'wqeew@WQer.com', 'ewfrfw'),
(41, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(42, '2', 'Support Ticket', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(43, '11', 'Support Ticket PHP Script', '', 'erte'),
(44, '2', 'Support Ticket PHP Script', '', 'sa'),
(45, '8', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(46, '8', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(47, '2', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(48, '11', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(49, '10', 'Support Ticket PHP Script', 'hello@domain.com', 'The status of this ticket has been changed to [Closed] if you need further help on this topic, open a new ticket.'),
(50, '10', 'Support Ticket PHP Script', 'hello@domain.com', 'We have reopened this message cdfgdgfdgd');

-- --------------------------------------------------------

--
-- Table structure for table `support_tickets`
--

CREATE TABLE IF NOT EXISTS `support_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `username` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `support_tickets`
--

INSERT INTO `support_tickets` (`id`, `subject`, `date`, `status`, `username`) VALUES
(1, 'ewwfgw', 'Fri, 01 Sep 2017 18:37:34 -0400', 'closed', 'admin'),
(2, 'ewnfof', 'Fri, 01 Sep 2017 19:42:51 -0400', 'open', 'admin'),
(8, 'fdb', 'Sun, 03 Sep 2017 10:22:09 -0400', 'closed', 'y3c3Y'),
(4, 'my device is broken', 'Sat, 02 Sep 2017 07:00:56 -0400', 'closed', 'admin'),
(9, 'fgefgfrew', 'Sun, 03 Sep 2017 10:54:29 -0400', 'closed', '1Dhxs'),
(10, 'fd', 'Sun, 03 Sep 2017 10:56:17 -0400', 'open', '1Dhxs'),
(11, 'ferwrf', 'Sun, 03 Sep 2017 10:59:33 -0400', 'closed', '1Dhxs');

-- --------------------------------------------------------

--
-- Table structure for table `support_users`
--

CREATE TABLE IF NOT EXISTS `support_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(12) NOT NULL,
  `password` varchar(64) NOT NULL,
  `admin` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `support_users`
--

INSERT INTO `support_users` (`id`, `username`, `password`, `admin`) VALUES
(1, 'admin', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 1),

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
