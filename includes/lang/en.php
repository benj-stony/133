<?php

	$l = array
	(
	"Status"=>"Status",
	"Subject"=>"Subject",
	"From"=>"From",
	"TicketOpened"=>"Ticket Opened",
	"OriginalMessage"=>"Original Message",
	"Reply"=>"Reply",
	"Replies"=>"Replies",
	"YourName"=>"Name",
	"YourEmail"=>"Email Address",
	"TypeMessage"=>"Description",
	"Submit"=>"Submit",
	"SubmitReply"=>"Submit Reply",
	"Back"=>"Back to tickets",
	"BackOpen"=>"Back to open tickets",
	"BackClosed"=>"Back to closed tickets",
	"Notice"=>"Notice",
	"NoOpen"=>"There are currently no open tickets.",
	"NoClosed"=>"There are currently no closed tickets.",
	"Open"=>"Open Tickets",
	"Closed"=>"Closed Tickets",
	"Username"=>"Username",
	"Password"=>"Password",
	"Error"=>"Error",
	"Login"=>"Login",
	"LoginError"=>"Username &amp; Password do not match.",
	"Message"=>"Message",
	"Messages"=>"Messages",
	"SendMessage"=>"Submit",
	"Thanks"=>"Thanks",
	"TicketUser"=>"Tickets for user ",
	"Notice"=>"Notice",
	"NoTickets"=>" You have no tickets.",
	"Logout"=>"Logout",
	"NewTicket"=>"Create a new ticket"
	);
	
?>