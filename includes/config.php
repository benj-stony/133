<?php
	session_start();

	/* Admin Email */
	$stemail = "info@stonydesign.ch";

	/* Company Name */
  	$companyName = "Support Ticket - M133";

	/* Path where script is installed (with end /) */
  	$path = "http://133.stonydesign.ch/";

  	/* email@{putyourdomainhere} no slashes */
  	$domainsingle = "smtp.x10host.com";

	/* Version of Support Ticket*/
	$stversion = '2.1';

	/* Theme color*/
	$themecolor = 'blue';

	/* Alert you if an update is available? */
	$checkUpdate = 0;

	/* Enable file transfers/attachments? If 1 the folder must be CHMOD to 777*/
	$Attachments = 1;
	$uploadPath = 'uploads/';

	/* File types allowed to be uploaded by each type of user */
	$userUploads = array('jpg','png','gif','jpeg');
	$adminUploads = array('jpg','png','gif','jpeg','txt, zip');

	/* Allow BBCode in the replies? */
	$BBCode = 1;

	/* Send the admin an email when a ticket is created? */
  	$adminEmail = 1;

	/* Choose the language (add more in includes/lang/) en=english */
	$myLang = 'en';

	$servername = "localhost";
$username = "stonyde_133";
$password = "yLcv0%73";
$dbname= "stonyde_133";

try {
    $dbh= new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    #echo "Connected successfully";
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }

	#################################################################
	#	Leave everything below unless you know what you are doing!	#
	#################################################################


	/* Just a simple escape function */
	function hs($s)
	{
		return strip_tags(htmlspecialchars($s, ENT_QUOTES));
	}

	/* Used for generating usernames and file names */
	function genName($len = 5, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
	{
		$num_chars = strlen($chars);
		$ret = '';

		for($i = 0; $i < $len; ++$i)
		{
			$ret .= $chars[mt_rand(0, $num_chars)];
		}
	return $ret;
	}

	/* Generate the unique file name for attachment */
	function fileName($len = 6, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-')
	{
		$num_chars = strlen($chars);
		$ret = '';

		for($i = 0; $i < $len; ++$i)
		{
			$ret .= $chars[mt_rand(0, $num_chars)];
		}
	return $ret;
	}

	/* Simple email validation */
	function isEmail($email)
	{
		if(preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/* Used to include BBCode, you can add more if you need */
	function addBBCode($message)
	{
		$htmlMessage = preg_replace('#\[b\](.+)\[\/b\]#iUs', '<b>$1</b>', $message); //Bold [b][/b]
		$htmlMessage = preg_replace('#\[i\](.+)\[\/i\]#iUs', '<i>$1</i>', $htmlMessage); //Italic [i][/i]
		$htmlMessage = preg_replace('#\[u\](.+)\[\/u\]#iUs', '<u>$1</u>', $htmlMessage); //Underline [u][/u]
		$htmlMessage = preg_replace('#\[s\](.+)\[\/s\]#iUs', '<s>$1</s>', $htmlMessage); //Strike [s][/s]
		$htmlMessage = preg_replace('#\[a\](.+)\[\/a\]#iUs', '<a target="_blank" href="$1">$1</a>', $htmlMessage); //Link [a][/a]
		$htmlMessage = preg_replace('#\[list\](.+)\[\/list\]#iUs', '<li>$1</li>', $htmlMessage); //List [list][/list]
		$htmlMessage = preg_replace('#\[img\](.+)\[\/img\]#iUs', '<a target="_blank" href="$1"><img width="100%" src="$1"></a>', $htmlMessage); //Images [img][/img]

	return $htmlMessage;
	}

?>
