<?php
	require_once('includes/config.php');
	include 'includes/lang/' . $myLang . '.php';

	$ticketID = $_POST['ticket_id'];
	$replyMessage = hs(addslashes($_POST['reply']));
	$replyEmail = hs($_POST['from']);
	$replyName = hs($_POST['fromn']);
	
	/* Upload attachment if it's enabled and there is a file */
	if (!empty($_FILES) && $Attachments && !empty($_FILES['file']['tmp_name']))
	{
		$oldFile = basename($_FILES['file']['name']);
		$ipAddress = $_SERVER['REMOTE_ADDR'];
		
		//Get the files extension
		$getExt = pathinfo($oldFile);
		$checkExt = strtolower($getExt['extension']);
		
		//Check if the files extension is okay
		if (!in_array($checkExt, $userUploads))
		{   
			//Check for images
			echo "File-type is not allowed!";
			die;
		}
		
		//Create the new file name
		$newFile = genName() . "." . $checkExt;
		
		$uploadedPath = $uploadPath . $newFile;
		
		//The time
		$theTime = time();
		
		if (is_uploaded_file($_FILES['file']['tmp_name']))
		{
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadedPath))
			{
				/* Insert file info into database */
				$data = array( 'oldfile' => $oldFile, 'newfile' => $newFile, 'uploaderip' => $ipAddress, 'timestamp' => $theTime, 'ticket' => $ticketID );
				$stmt = $dbh->prepare("INSERT INTO support_files (oldfile, newfile, uploaderip, timestamp, ticket) VALUES (:oldfile, :newfile, :uploaderip, :timestamp, :ticket)");
				$stmt->execute($data);
			}
		}
	}
	
	if (empty($replyMessage))
	{
		$fError = 1;
	}
	
	$simpleHash = sha1($ticketID . $companyName);
	$tKey = base64_encode($ticketID);
	if (!$fError)
	{
		$userMessage = "\nHi,\n\nA customer has posted a reply in Support Ticket .\n\nView Ticket:\n" . $path . "admin/ticket.php?id=$ticketID\n\nThanks,\n" . $companyName . "\n";
		
		/* Insert the reply into the database */
		$data = array( 'on_ticket' => $ticketID, 'from_name' => $replyName, 'from_email' => $replyEmail, 'content' => $replyMessage );
		$stmt = $dbh->prepare("INSERT INTO support_messages (on_ticket, from_name, from_email, content) VALUES (:on_ticket, :from_name, :from_email, :content)");
		$stmt->execute($data);

		if($adminEmail)
		{
			mail($stemail, "Ticket Updated", $userMessage, "From: Support Ticket <no-reply@"$domainsingle">");
		}
		
		header("Location: index.php?t=$tKey&h=$simpleHash");
	}
	else
	{
		header("Location: index.php?t=$tKey&h=$simpleHash&fe=1");
	}
?>