<?php
	include 'includes/config.php';
	include 'includes/lang/' . $myLang . '.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Submit a ticket</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
           <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">

	</head>

	<body oncontextmenu="return false">


	<div class="ui vertical stripe segment piled">
			<div class="ui container">

			<div class="ui tiny breadcrumb">
  <a href="<?php echo $path; ?>" class="section">Home</a>
  <i class="right chevron icon divider"></i>
  <div class="active section">Submit a ticket</div>
</div>

			<div class="ui info message">
  <div class="header">
    Things to know about <?php echo $companyName; ?>
  </div>
  <ul class="list">
    <li>You can't upload files until your ticket is opened by an operator!</li>
    <li>Tickets are automatically closed after 20 days if there is no activity.</li>
  </ul>
</div>

			  <div class="ui huge header">Submit a ticket!</div>
			<form class="ui form makeForm" method="post" action="new.php">
  <div class="field required">
    <label><?php echo $l['YourName']?></label>
    <input type="text" name="name" placeholder="Name...">
  </div>
  <div class="field required">
    <label><?php echo $l['YourEmail']?></label>
    <input type="email" name="email" placeholder="Email...">
  </div>
    <div class="field required">
    <label><?php echo $l['Subject']?></label>
    <input type="text" name="subject" placeholder="What is your ticket about...">
  </div>

	<div class="field required">
	<label><?php echo $l['Importancy']?>Is it important?</label>
	<input type="radio" name="importancy" value="important" checked="checked"/>important<br />
	<input type="radio" name="importancy" value="unimportant"/>unimportant<br />
</div>

<link href="css/switch.css" rel="stylesheet">
      <div class="field required">
    <label><?php echo $l['TypeMessage']?></label>
    <textarea name="message" placeholder="Description regarding your issue..."></textarea>
</div>

  <button class="ui button blue" type="submit"><?php echo $l['SendMessage']?></button>
</form>
	</div>
	</div>
	</body>
</html>
