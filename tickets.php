<?php
	require_once('includes/config.php');
	include 'includes/lang/' . $myLang . '.php';

	/* Check login */
	$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
	if(!isset($_SESSION["keyTicket_$myHash"]))
	{
		header("Location: login");
	}

	$userName = $_SESSION['Username'];

?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Support Center</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
     <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">

	</head>

	<body oncontextmenu="return false">

	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    <div class="item">
        <a class="ui white button" href="pwchange.php">Change password</a>
    </div>
        <div class="item mobile hidden">
        <a class="modalHandle">Sign Out</a>
    </div>
  </div>
</div>

	<div class="ui inverted vertical masthead center aligned segment <?php echo $themecolor; ?>">


    <div class="ui text container">
      <h1 class="ui inverted header">
        My Tickets
      </h1>
      <a href="submit" class="ui white button">
<?php echo $l['NewTicket']; ?>
</a>
    </div>

  </div>




	<div class="ui vertical stripe segment">
  <div class="ui container">

      <table class="ui celled table">
  <thead>
    <tr>
      <th>Subject</th>
      <th>Created</th>
      <th>By</th>
      <th>Replies</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
<?php

                    /* Ticket Info */
                    $stmt = $dbh->prepare("SELECT * FROM support_tickets WHERE username = :username ORDER BY id DESC");
                    $stmt->bindParam(':username', $userName);
                    $stmt->execute();
                    $tickets = $stmt->fetchAll(PDO::FETCH_ASSOC);
#var_dump($tickets);
                    foreach ($tickets as $myticket)
                    {
                        $ticket_id = $myticket['id'];
                        $stmt = $dbh->prepare("SELECT * FROM support_messages WHERE on_ticket = $ticket_id");

                        $stmt->execute();
                        $getReplies = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        $replyNo = count($getReplies);
                        $simpleHash = sha1($ticket_id . $companyName);

                        echo '<tr>';
                        echo '<td><a href="index.php?t=' . base64_encode($myticket['id']) . '&h=' . $simpleHash . '">' . $myticket['subject'] . '</a></td><td>' . $myticket['date'] . '</td><td>' . $userName . '</td> <td>' . $replyNo . '</td> <td>' . $myticket['status'] . '</td>';
                        echo '</tr>';
                    }

                ?>
  </tbody>
</table>

      	<?php
      		if(count($tickets) == 0)
					{
						echo '<div class="ticket"><b>' . $l['Notice'] . ':</b> ' . $l['NoTickets'] . '</div>';
					}

				?>




  </div>
</div>
	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>

        <div class="ui tiny modal custom_login_modal">
  <i class="close icon"></i>
  <div class="header ui center aligned custom_login_modal_header">
    See ya another time!
  </div>
  <div class="description">
    <br>
    <br>


      <a href="<?php echo $path; ?>main.php?logout=true" class="fluid ui large button <?php echo $themecolor; ?>">Sign out!</a>
      <br>

  </div>
</div>
<script>
  //modal
  $(".modalHandle").click(function() {
    $(".ui.modal")
      .modal({
        blurring: false,
        duration: 200
      })
      .modal("show");
  });
</script>
	</body>
</html>
