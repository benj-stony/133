<?php
	include 'includes/config.php';
	include 'includes/lang/' . $myLang . '.php';
?>
<?php
	if (!empty($_GET['logout']))
	{
		$_SESSION = array();
		session_destroy();
	}
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		/* Escape the username, and hash the password */
		$myUser = $_POST['username'];
		$myPass = sha1($_POST['password']);

		$stmt = $dbh->prepare("SELECT username, password, admin FROM support_users WHERE username = :username AND password = :password");
		$stmt->bindParam(':username', $myUser);
		$stmt->bindParam(':password', $myPass);
		$stmt->execute();
		$loginDetails = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$adminUser = $loginDetails[0]['admin'];
		if (count($loginDetails) == 1 && $myUser == $loginDetails[0]['username'] && $myPass == $loginDetails[0]['password'])
		{
			$_SESSION['Username'] = $myUser;
			$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
			$_SESSION["keyTicket_$myHash"] = md5(sha1($_SESSION['Username']));

			if ($adminUser == 1)
			{
				/* Can view the admin area */
				$_SESSION['Admin'] = 1;
				header("Location: admin/index.php");
			}
			else
			{
				/* Area to view the users tickets */
				header("Location: tickets.php");
			}
		}
		else
		{
			$logError = 'Username &amp; Password do not match.';
			echo $loginDetails['username'];
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Support Center</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
       <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">

	</head>

	<body oncontextmenu="return false">

	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    <div class="item">
        <a href="/login" class="ui button ">Sign In</a>
    </div>
        <div class="item mobile hidden">
        <a href="submit">Submit a ticket</a>
    </div>
  </div>
</div>

	<div class="ui inverted vertical masthead center aligned segment <?php echo $themecolor; ?>">


    <div class="ui text container">
      <h1 class="ui inverted header">
        <?php echo $companyName; ?>
      </h1>
      <a href="submit" class="ui white button">
  Submit a ticket!
</a>
    </div>

  </div>

	<div class="ui vertical stripe segment">
  <div class="ui container">


      <div class="ui three column grid center aligned stackable">
<div class="column">
      <div class="ui secondary segment custom_blockpadding">
    <h2 class="ui center large aligned header">
  What can I ask?
</h2>
    <p>
      You can ask anything you want (as long as its related to a service we offer!)
    </p>
  </div>
  </div>
<div class="column">
 <div class="ui secondary segment custom_blockpadding">
    <h2 class="ui center large aligned header">
  Response Times
</h2>
    <p>
      We usually get back to you within 24 hours but it can sometimes be 48!
    </p>
    </div>
  </div>
<div class="column">
 <div class="ui secondary segment custom_blockpadding">
    <h2 class="ui center large aligned header">
  Proffessionals Only!
</h2>
    <p>
      When you submit a ticket it will be read by a pro so it should get sorted faster!
    </p>
    </div>
  </div>
</div>

  </div>
</div>


<link rel="stylesheet" type="text/css" href="css/faq.css">
<div style="padding: 5%">


<h2>Frequently Asked Questions</h2>

<button class="accordion">My Windows 7 product key won't verify. What's the problem?</button>
<div class="panel">
  <p>The most common issue is the use of a product key for a product not currently supported by the site such as an Upgrade key, an MSDN key, product keys for pre-installed media or an Enterprise edition key. For access to MSDN products or Enterprise edition visit the MSDN Portal or the Volume Licensing Service Center. Upgrades and pre-installed media are not currently supported by the tools on the site.
If you believe you have a valid product key and are still receiving an error, please contact Support.</p>
</div>

<button class="accordion">I purchased my copy of Windows through a university. Can I download it here?</button>
<div class="panel">
  <p>Yes, but you’ll need your product key. Go to the Academic Products download page and select the version of Windows to begin.</p>
</div>

<button class="accordion">I have a non-Windows device (like an Apple) and I want to run Windows on it. Where do I get the media?</button>
<div class="panel">
  <p>You can download an ISO file copy of Windows to use with a non-Windows device. Follow these links for Windows 10, Windows 8.1 or Windows 7 ISO download options best for non-Windows devices.</p>
</div>

<script type='text/javascript' src='js/faq.js'></script>
</div>




	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>

        <div class="ui tiny modal custom_login_modal">
  <i class="close icon"></i>
  <div class="header ui center aligned custom_login_modal_header">
    Great to have you back!
  </div>
  <div class="description">
    <br>
    <br>

    <?php
					if(isset($logError))
					{
						echo '<div class="error"><b>Error:</b> ' . $logError . '</div>';
					}
				?>
				<form method="post" action="" accept-charset="utf-8">
				      <div class="ui fluid icon input">
        <input type="text" name="username" placeholder="Username">
      </div>
      <br>
      <div class="ui fluid icon input">
        <input type="password" name="password" placeholder="Password">
      </div>
<br>
      <button type="submit" name="submit" class="fluid ui large button <?php echo $themecolor; ?>">Sign In</button>
      <br>
				</form>

  </div>
</div>
<script>
  //modal
  $(".modalHandle").click(function() {
    $(".ui.modal")
      .modal({
        blurring: false,
        duration: 200
      })
      .modal("show");
  });
</script>
	</body>
</html>
