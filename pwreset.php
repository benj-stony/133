<html>
<head>
<title>Change Password</title>
<link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>
<form name="frmChange" method="post" action="" onSubmit="return validatePassword()">
<div style="width:500px;">
<div class="message"><?php if(isset($message)) { echo $message; } ?></div>
<table border="0" cellpadding="10" cellspacing="0" width="500" align="center" class="tblSaveForm">
<tr class="tableheader">
<td colspan="2">Change Password</td>
</tr>
<tr>
<td width="40%"><label>Current Password</label></td>
<td width="60%"><input type="password" name="currentPassword" class="txtField"/><span id="currentPassword"  class="required"></span></td>
</tr>
<tr>
<td><label>New Password</label></td>
<td><input type="password" name="newPassword" class="txtField"/><span id="newPassword" class="required"></span></td>
</tr>
<td><label>Confirm Password</label></td>
<td><input type="password" name="confirmPassword" class="txtField"/><span id="confirmPassword" class="required"></span></td>
</tr>
<tr>
<td colspan="2"><input type="submit" name="submit" value="Submit" class="btnSubmit"></td>
</tr>
</table>
</div>
</form>
</body></html>


<html>
	<head>
		<title><?php echo $companyName; ?> | Change Password</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
           <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">
	</head>
	<body>

		<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
    <div class="item">
        <a class="ui <?php echo $themecolor; ?> button modalHandle">Sign In</a>
    </div>
        <div class="item mobile hidden">
        <a href="submit">Submit a ticket</a>
    </div>
  </div>
</div>


      <div class="ui text container centercenter ">
        	<?php
					if(isset($logError))
					{
						echo '<div class="ui red message"><strong>Error</strong> ' . $logError . '</div>';
					}
				?>
        <form method="post" action="" class="ui large form">
          <div class="field">
            <div class="ui left icon input">
              <i class="user icon"></i><input name="username" placeholder="Username..." type="text" />
            </div>
          </div>
          <div class="field">
            <div class="ui left icon input">
              <i class="lock icon"></i><input name="password" placeholder="Password..." type="password" />
            </div>
          </div>

          <button type="submit" class="ui fluid large blue submit button">
            Change password
          </button>
        </form>
      </div>

    	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>


	    <style type="text/css">
      body {
        background-color: #eee;
      }
            .ui.footer.segment {
        width: 100%;
        margin-bottom: 0;
        margin-top:0;
        padding:20px;
        background-color: #1b1c1d;
        position: fixed;
        bottom: 0;
      }
    </style>
		   <script>
      $(document)
        .ready(function() {
          $('.ui.checkbox').checkbox();
          $('.ui.form')
            .form({
              fields: {
                email: {
                  identifier  : 'username',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your username'
                    }
                  ]
                },
                password: {
                  identifier  : 'password',
                  rules: [
                    {
                      type   : 'empty',
                      prompt : 'Please enter your password'
                    },
                    {
                      type   : 'length[4]',
                      prompt : 'Your password must be at least 4 characters'
                    }
                  ]
                }
              },
              inline: true,
              on: 'blur'
            })
          ;
        })
      ;
    </script>
	</body>
</html>
