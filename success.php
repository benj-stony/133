<?php
	include 'includes/config.php';
	include 'includes/lang/' . $myLang . '.php';
?>
<?php
	if (!empty($_GET['logout']))
	{
		$_SESSION = array();
		session_destroy();
	}
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		/* Escape the username, and hash the password */
		$myUser = $_POST['username'];
		$myPass = sha1($_POST['password']);

		$stmt = $dbh->prepare("SELECT username, password, admin FROM support_users WHERE username = :username AND password = :password");
		$stmt->bindParam(':username', $myUser);
		$stmt->bindParam(':password', $myPass);
		$stmt->execute();
		$loginDetails = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$adminUser = $loginDetails[0]['admin'];
		if (count($loginDetails) == 1 && $myUser == $loginDetails[0]['username'] && $myPass == $loginDetails[0]['password'])
		{
			$_SESSION['Username'] = $myUser;
			$myHash = md5($_SERVER['REMOTE_ADDR'] . date("dmY"));
			$_SESSION["keyTicket_$myHash"] = md5(sha1($_SESSION['Username']));

			if ($adminUser == 1)
			{
				/* Can view the admin area */
				$_SESSION['Admin'] = 1;
				header("Location: admin/index.php");
			}
			else
			{
				/* Area to view the users tickets */
				header("Location: tickets.php");
			}
		}
		else
		{
			$logError = 'Username &amp; Password do not match.';
			echo $loginDetails['username'];
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title><?php echo $companyName; ?> | Support Center</title>
  <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.11/semantic.min.js"></script>
       <link rel="stylesheet" type="text/css" href="<?php echo $path; ?>css/styles.css">

	</head>

	<body oncontextmenu="return false">

	<div class="ui menu custom_nomargin">
    <div class="item">
       <strong> <a href="<?php echo $path; ?>"><?php echo $companyName; ?></a> </strong>
    </div>
    <div class="right menu">
  </div>
</div>

<div style="padding-left: 30%; padding-right: 30%">
<h1>You changed your password successfully</h1>
<a href="/tickets.php"><button type="submit" class="ui fluid large blue submit button">
	Back to your tickets
</button></a>

</div>


	    <div class="ui footer basic <?php echo $themecolor; ?> inverted segment">
      <div class="ui container center aligned">
<p>Copyright &copy; <?php echo date("Y"); ?> by Benjamin & Andreas | v<?php echo $stversion; ?></p>
      </div>
    </div>

        <div class="ui tiny modal custom_login_modal">
  <i class="close icon"></i>
  <div class="header ui center aligned custom_login_modal_header">
    Great to have you back!
  </div>
  <div class="description">
    <br>
    <br>

    <?php
					if(isset($logError))
					{
						echo '<div class="error"><b>Error:</b> ' . $logError . '</div>';
					}
				?>
				<form method="post" action="" accept-charset="utf-8">
				      <div class="ui fluid icon input">
        <input type="text" name="username" placeholder="Username">
      </div>
      <br>
      <div class="ui fluid icon input">
        <input type="password" name="password" placeholder="Password">
      </div>
<br>
      <button type="submit" name="submit" class="fluid ui large button <?php echo $themecolor; ?>">Sign In</button>
      <br>
				</form>

  </div>
</div>
<script>
  //modal
  $(".modalHandle").click(function() {
    $(".ui.modal")
      .modal({
        blurring: false,
        duration: 200
      })
      .modal("show");
  });
</script>
	</body>
</html>
